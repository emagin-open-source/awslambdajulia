IMAGE_NAME := julia_runtime:1.0.2

.PHONY: build-runtime

## Build runtime
build-runtime:
	@echo '$(GREEN)Building runtime...$(RESET)'
#	@mkdir -p $(CURDIR)/packaging/
#	@cp AWSLambdaJuliaRuntime/src/AWSLambdaJuliaRuntime.jl $(CURDIR)/packaging/
	@docker build \
		-t julia_runtime:1.0.2 .
#	-@rm -rf $(CURDIR)/packaging/
	@docker run --rm -it -v "$(CURDIR)/packaging:/var/host" $(IMAGE_NAME) zip --symlinks -r -9 /var/host/julia-v1.zip .

################################ HELPER TARGETS - DO NOT EDIT #############################
## `help` target will show description of each target
## Target description should be immediate line before target starting with `##`

# COLORS
RED    := $(shell tput -Txterm setaf 1)
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

TARGET_MAX_CHAR_NUM=20
## Show help
help:
	@echo ''
	@echo 'Usage:'
	@echo '  $(YELLOW)make$(RESET) $(GREEN)<target>$(RESET)'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			split($$1, arr, ":"); \
			helpCommand = arr[1]; \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  $(YELLOW)%-$(TARGET_MAX_CHAR_NUM)s$(RESET) $(GREEN)%s$(RESET)\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

.PHONY: help

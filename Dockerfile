FROM lambci/lambda:build-python3.7  

RUN mkdir /var/src 

RUN yum install -y gcc wget tar
 
RUN cd /var/src \
 && wget https://julialang-s3.julialang.org/bin/linux/x64/1.0/julia-1.0.2-linux-x86_64.tar.gz \
 && tar -xzvf julia-1.0.2-linux-x86_64.tar.gz
 
RUN cp -R /var/src/julia-1.0.2/** /var/task/

RUN rm -rf /var/src

RUN ls /var/task/
 
ENV PATH=/var/task/bin:$PATH

# start fresh again 
FROM lambci/lambda:build-python3.7
 
COPY --from=0 /var/task/ /var/task/
 
ENV PATH=/var/task/bin:$PATH

# copy relevant file for package installation
COPY Project.toml .
COPY Manifest.toml .
COPY src/ src/
COPY REQUIRE .

# install packages
RUN julia -e 'using Pkg; Pkg.activate("."); Pkg.instantiate(); \
                for p in eachline("REQUIRE") \
                    Pkg.add("$p")            \
                end'

# An example that it works
RUN julia -e 'using Pkg; Pkg.activate("."); include("src/awslambdajulia.jl"); println("hello julia")'

# Get latest AWSLambdaJuliaRuntime.jl module
COPY src/awslambdajulia.jl \
    /var/task/share/julia/

# Recompile sys.so with required packages.
#RUN mkdir -p /tmp/julia/sys                                                 && \
#    julia -e '                                                                 \
#        open("/tmp/julia/userimg.jl", "w") do f;                               \
#            println(f,"using AWSLambdaJuliaRuntime");                          \
#            for p in eachline("REQUIRE")                                       \
#                println(f, "using $p")                                         \
#            end                                                                \
#        end'

#RUN julia -e 'using Pkg; Pkg.add("PackageCompiler")'

#RUN julia -e 'using Pkg; Pkg.instantiate()'

#RUN julia -e 'using PackageCompiler; PackageCompiler.build_shared_lib("/tmp/julia/userimg.jl")'

RUN ls /root/.julia/packages

RUN cp -R /root/.julia/packages/** /var/task/share/julia/

# Remove unnecessary files.
RUN find /var/task/ -name '.git' \
    -o -name '.cache' \
    -o -name '.travis.yml' \
    -o -name '.gitignore' \
    -o -name 'REQUIRE' \
    -o -name 'LICENSE' \
    -o -name 'test' \
    -o -path '*/deps/usr/downloads' \
    -o -path '*/deps/usr/manifests' \
    -o -path '*/deps/downloads' \
    -o -path '*/deps/builds' \
    -o -path '*/deps/src' \
    -o -path '*/deps/usr/logs' \
    -o -path '*/JSON/data' \
    -o \( -type f -path '*/deps/src/*' ! -name '*.so.*' \) \
    -o \( -type f -path 'julia/*' -name '*.jl' \) \
    -o -path '*/deps/usr/include' \
    -o -path '*/deps/usr/bin' \
    -o -path '*/deps/usr/lib/*.a' \
    -o -name 'doc' \
    -o -name 'docs' \
    -o -name 'examples' \
    -o -name '*.md' \
    -o -name '*.yml' \
    -o -name '*.toml' \
    -o -name '*.tar.gz' \
    -o -name 'METADATA' \
    -o -name 'META_BRANCH' \
    | xargs rm -rf


# Install bootstrap
COPY bootstrap /var/task/bootstrap

RUN mkdir /var/task/share/julia/awslambdajulia

COPY Project.toml /var/task/share/julia/awslambdajulia
COPY Manifest.toml /var/task/share/julia/awslambdajulia
COPY src/ /var/task/share/julia/awslambdajulia/src/

RUN rm -f                                       lib/julia/*-debug.so*       && \
    rm -f                                       lib/*-debug.so* 

# FIXME Remove "|| true" When MbedTLS stripping fix is released.
# https://github.com/JuliaWeb/MbedTLS.jl/issues/140
RUN for f in $(find . -name '*.so'); do strip $f; done

